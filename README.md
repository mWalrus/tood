# Tood: a simple CLI todo manager

![Demo](./media/tood-demo.gif)

## Install
1. `git clone https://gitlab.com/mWalrus/tood.git`
2. `cd tood`
3. `make all`
4. `tood`.

